<?php
/**
 * @file
 * Group user member UI.
 */

/**
 * Replace use forms with guser forms. Based on user_admin.
 *
 * Builds the group user overview form.
 *
 * @see user_admin()
 */
function guser_user_admin($group) {
  module_load_include('inc', 'user', 'user.admin');

  if (!empty($_POST['accounts']) && isset($_POST['operation']) && ($_POST['operation'] == 'cancel')) {
    $build['user_multiple_cancel_confirm'] = drupal_get_form('guser_user_multiple_cancel_confirm', $group);
  }
  else {
    $build['user_filter_form'] = drupal_get_form('user_filter_form', $group);
    $build['user_admin_account'] = drupal_get_form('guser_user_admin_account', $group);
  }

  return $build;
}

/**
 * Build the guser user management page. Based on user_admin_account.
 *
 * The query has been changed to select users based on the group permissions
 * 'edit user' and 'edit member user', and the submit function has been set
 * to the default user_admin_account_submit.
 *
 * @see user_admin_account()
 * @see user_admin_account_validate()
 * @see user_admin_account_submit()
 */
function guser_user_admin_account($form, &$form_state) {
  // Get the group.
  $group = $form_state['build_info']['args'][0];
  $gid = $group->gid;

  $header = array(
    'username' => array('data' => t('Username'), 'field' => 'u.name'),
    'status' => array('data' => t('Status'), 'field' => 'u.status'),
    'roles' => array('data' => t('Roles')),
    'member_for' => array(
      'data' => t('Member for'),
      'field' => 'u.created',
      'sort' => 'desc',
    ),
    'access' => array('data' => t('Last access'), 'field' => 'u.access'),
    'operations' => array('data' => t('Operations')),
  );

  // Check the user access.
  $emu = group_access('edit member user', $group);
  $eu = group_access('edit user', $group);
  $view = group_access('view user', $group);

  // Query to get all users that are members.
  $emu_query = db_select('users', 'u1');
  $emu_query->join('group_membership', 'gm', 'u1.uid = gm.uid AND gm.gid = :gid', array(':gid' => $gid));
  $emu_query->fields('u1');

  // Query to get all users that are children.
  $eu_query = db_select('users', 'u2');
  $eu_query->join('group_entity', 'g', 'u2.uid = g.entity_id AND g.gid = :gid AND g.entity_type = :type', array(
    ':gid' => $gid,
    ':type' => 'user',
  ));
  $eu_query->fields('u2');

  // Build the query based on permissions, keep u as the alias so nothing else
  // has to be updated.
  if ($emu && $eu || $view) {
    $query = db_select($emu_query->union($eu_query), 'u');
  }
  else {
    if ($emu) {
      $query = db_select($emu_query, 'u');
    }
    else {
      if ($eu) {
        $query = db_select($eu_query, 'u');
      }
    }
  }

  user_build_filter_query($query);

  $count_query = clone $query;
  $count_query->addExpression('COUNT(u.uid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('u', array('uid', 'name', 'status', 'created', 'access'))
    ->limit(50)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#attributes' => array('class' => array('container-inline')),
    '#access' => $eu || $emu,
  );

  $options = array();
  foreach (module_invoke_all('user_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }

  // Unset options that the user does not have permission to use.
  if (!group_access('edit user status', $group) && !group_access('delete user', $group)) {
    unset($options['unblock']);
    unset($options['block']);
  }

  if (!group_access('delete user', $group)) {
    unset($options['cancel']);
  }

  $form['options']['operation'] = array(
    '#type' => 'select',
    '#title' => t('Operation'),
    '#title_display' => 'invisible',
    '#options' => $options,
    '#default_value' => 'unblock',
  );
  $options = array();
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
  );

  $destination = drupal_get_destination();

  $status = array(t('blocked'), t('active'));
  $roles = array_map('check_plain', user_roles(TRUE));

  foreach ($result as $account) {
    $users_roles = array();
    $roles_result = db_query('SELECT rid FROM {users_roles} WHERE uid = :uid', array(':uid' => $account->uid));
    foreach ($roles_result as $user_role) {
      $users_roles[] = $roles[$user_role->rid];
    }
    asort($users_roles);

    $edit_access = guser_access_callback('edit user', $group, $account);

    // Change the edit page to the guser edit page.
    $options[$account->uid] = array(
      'username' => theme('username', array('account' => $account)),
      'status' => $status[$account->status],
      'roles' => theme('item_list', array('items' => $users_roles)),
      'member_for' => format_interval(REQUEST_TIME - $account->created),
      'access' => $account->access ? t('@time ago', array('@time' => format_interval(REQUEST_TIME - $account->access))) : t('never'),
      '#disabled' => !$edit_access,
      'operations' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('edit'),
          '#href' => "group/$group->gid/member/$account->uid/edit-user",
          '#options' => array('query' => $destination),
          '#access' => $edit_access,
        ),
      ),

    );
  }

  $form['accounts'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No people available.'),
    '#disabled' => !$form['options']['#access'],
    '#js_select' => $form['options']['#access'],
    '#process' => array(
      'form_process_tableselect',
      'guser_tableselect_process',
    ),
  );
  $form['pager'] = array('#markup' => theme('pager'));

  // Add the default user submit function.
  $form['#submit'][] = 'user_admin_account_submit';

  $form['#validate'][] = 'guser_user_admin_account_validate';

  // Add the default user validation function.
  $form['#validate'][] = 'user_admin_account_validate';

  return $form;
}

/**
 * Validation function to make sure users can't block themselves.
 */
function guser_user_admin_account_validate($form, &$form_state) {
  global $user;

  if ($form_state['values']['operation'] == 'block') {
    $form_state['values']['accounts'][$user->uid] = NULL;
  }
}

/**
 * Based on user_multiple_cancel_confirm. Changed the redirect paths.
 *
 * @see user_multiple_cancel_confirm()
 */
function guser_user_multiple_cancel_confirm($form, &$form_state) {
  $gid = $form_state['build_info']['args'][0]->gid;

  $edit = $form_state['values'];

  $form['accounts'] = array(
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );
  $accounts = user_load_multiple(array_keys(array_filter($edit['accounts'])));
  foreach ($accounts as $uid => $account) {
    // Prevent user 1 from being canceled.
    if ($uid <= 1) {
      continue;
    }
    $form['accounts'][$uid] = array(
      '#type' => 'hidden',
      '#value' => $uid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($account->name) . "</li>\n",
    );
  }

  // Output a notice that user 1 cannot be canceled.
  // Change the redirect path to one better suited for guser.
  if (isset($accounts[1])) {
    $redirect = (count($accounts) == 1);
    $message = t('The user account %name cannot be cancelled.', array('%name' => $accounts[1]->name));
    drupal_set_message($message, $redirect ? 'error' : 'warning');
    // If only user 1 was selected, redirect to the overview.
    if ($redirect) {
      drupal_goto("group/$gid/member/user-management");
    }
  }

  $form['operation'] = array('#type' => 'hidden', '#value' => 'cancel');

  module_load_include('inc', 'user', 'user.pages');
  $form['user_cancel_method'] = array(
    '#type' => 'item',
    '#title' => t('When cancelling these accounts'),
  );
  $form['user_cancel_method'] += user_cancel_methods();
  // Remove method descriptions.
  foreach (element_children($form['user_cancel_method']) as $element) {
    unset($form['user_cancel_method'][$element]['#description']);
  }

  // Allow to send the account cancellation confirmation mail.
  $form['user_cancel_confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require e-mail confirmation to cancel account.'),
    '#default_value' => FALSE,
    '#description' => t('When enabled, the user must confirm the account cancellation via e-mail.'),
  );
  // Also allow to send account canceled notification mail, if enabled.
  $form['user_cancel_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when account is canceled.'),
    '#default_value' => FALSE,
    '#access' => variable_get('user_mail_status_canceled_notify', FALSE),
    '#description' => t('When enabled, the user will receive an e-mail notification after the account has been cancelled.'),
  );

  // Change the redirect path to one better suited for guser.
  return confirm_form($form,
    t('Are you sure you want to cancel these user accounts?'),
    "group/$gid/member/user-management", t('This action cannot be undone.'),
    t('Cancel accounts'), t('Cancel'));
}

/**
 * Based on user_multiple_cancel_confirm_submit. Changed the redirect path.
 *
 * Submit handler for mass-account cancellation form.
 *
 * @see guser_user_multiple_cancel_confirm()
 * @see user_multiple_cancel_confirm()
 * @see user_cancel_confirm_form_submit()
 */
function guser_user_multiple_cancel_confirm_submit($form, &$form_state) {
  global $user;
  $gid = $form_state['build_info']['args'][0]->gid;

  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['accounts'] as $uid => $value) {
      // Prevent programmatic form submissions from cancelling user 1.
      if ($uid <= 1) {
        continue;
      }
      // Prevent user administrators from deleting themselves without
      // confirmation.
      if ($uid == $user->uid) {
        $admin_form_state = $form_state;
        unset($admin_form_state['values']['user_cancel_confirm']);
        $admin_form_state['values']['_account'] = $user;
        user_cancel_confirm_form_submit(array(), $admin_form_state);
      }
      else {
        user_cancel($form_state['values'], $uid, $form_state['values']['user_cancel_method']);
      }
    }
  }
  $form_state['redirect'] = "group/$gid/member/user-management";
}

/**
 * Changed the permission checks and redirects from user_cancel_confirm_form.
 *
 * @see user_cancel_confirm_form()
 * @see user_edit_cancel_submit()
 */
function guser_cancel_confirm_form($form, &$form_state, $account, $category, $group) {
  global $user;

  module_load_include('inc', 'user', 'user.pages');

  $form['_account'] = array('#type' => 'value', '#value' => $account);

  // Display account cancellation method selection, if allowed.
  $admin_access = group_access('delete user', $group);
  $can_select_method = $admin_access;
  $form['user_cancel_method'] = array(
    '#type' => 'item',
    '#title' => ($account->uid == $user->uid ? t('When cancelling your account') : t('When cancelling the account')),
    '#access' => $can_select_method,
  );
  $form['user_cancel_method'] += user_cancel_methods();

  // Allow user administrators to skip the account cancellation confirmation
  // mail (by default), as long as they do not attempt to cancel their own
  // account.
  $override_access = $admin_access && ($account->uid != $user->uid);
  $form['user_cancel_confirm'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require e-mail confirmation to cancel account.'),
    '#default_value' => ($override_access ? FALSE : TRUE),
    '#access' => $override_access,
    '#description' => t('When enabled, the user must confirm the account cancellation via e-mail.'),
  );
  // Also allow to send account canceled notification mail, if enabled.
  $default_notify = variable_get('user_mail_status_canceled_notify', FALSE);
  $form['user_cancel_notify'] = array(
    '#type' => 'checkbox',
    '#title' => t('Notify user when account is canceled.'),
    '#default_value' => ($override_access ? FALSE : $default_notify),
    '#access' => $override_access && $default_notify,
    '#description' => t('When enabled, the user will receive an e-mail notification after the account has been cancelled.'),
  );

  // Prepare confirmation form page title and description.
  if ($account->uid == $user->uid) {
    $question = t('Are you sure you want to cancel your account?');
  }
  else {
    $question = t('Are you sure you want to cancel the account %name?', array('%name' => $account->name));
  }
  $description = '';
  if ($can_select_method) {
    $description = t('Select the method to cancel the account above.');
    foreach (element_children($form['user_cancel_method']) as $element) {
      unset($form['user_cancel_method'][$element]['#description']);
    }
  }
  else {
    // The radio button #description is used as description for the confirmation
    // form.
    foreach (element_children($form['user_cancel_method']) as $element) {
      if ($form['user_cancel_method'][$element]['#default_value'] == $form['user_cancel_method'][$element]['#return_value']) {
        $description = $form['user_cancel_method'][$element]['#description'];
      }
      unset($form['user_cancel_method'][$element]['#description']);
    }
  }

  // Always provide entity id in the same form key as in the entity edit form.
  $form['uid'] = array('#type' => 'value', '#value' => $account->uid);
  return confirm_form($form,
    $question,
    "group/$group->gid/member/{$form['uid']['#value']}/edit-user",
    $description . ' ' . t('This action cannot be undone.'),
    t('Cancel account'), t('Cancel'));
}

/**
 * Changed permission checks and redirects from user_cancel_confirm_form_submit.
 *
 * @see guser_cancel_confirm_form()
 * @see user_cancel_confirm_form()
 * @see user_multiple_cancel_confirm_submit()
 * @see user_cancel_confirm_form_submit()
 */
function guser_cancel_confirm_form_submit($form, &$form_state) {
  global $user;
  $account = $form_state['values']['_account'];
  $group = $form_state['build_info']['args'][2];

  // Cancel account immediately, if the current user has administrative
  // privileges, no confirmation mail shall be sent, and the user does not
  // attempt to cancel the own account.
  if (group_access('delete user', $group) && empty($form_state['values']['user_cancel_confirm']) && $account->uid != $user->uid) {
    user_cancel($form_state['values'], $account->uid, $form_state['values']['user_cancel_method']);

    $form_state['redirect'] = "group/$group->gid/member/user-management";
  }
  else {
    // Store cancelling method and whether to notify the user in $account for
    // user_cancel_confirm().
    $edit = array(
      'user_cancel_method' => $form_state['values']['user_cancel_method'],
      'user_cancel_notify' => $form_state['values']['user_cancel_notify'],
    );
    $account = user_save($account, $edit);
    _user_mail_notify('cancel_confirm', $account);
    drupal_set_message(t('A confirmation request to cancel your account has been sent to your e-mail address.'));
    watchdog('user', 'Sent account cancellation request to %name %email.', array(
      '%name' => $account->name,
      '%email' => '<' . $account->mail . '>',
    ), WATCHDOG_NOTICE);

    $form_state['redirect'] = "group/$group->gid/member/{$account->uid}/edit-user";
  }
}

/**
 * Implements callback_form_element_process().
 */
function guser_tableselect_process(&$element, &$form_state, &$form) {
  // We have to process the #disabled property ourselves.
  // This can be removed when https://drupal.org/node/2101357 is fixed.
  foreach (element_children($element['#options']) as $key) {

    if (isset($element['#options'][$key]['#disabled'])) {
      $element[$key]['#disabled'] = $element['#options'][$key]['#disabled'];
    }
    else {
      $element[$key]['#disabled'] = FALSE;
    }
  }
  return $element;
}
