<?php
/**
 * @file
 * Provide views data and handlers for guser.
 */

/**
 * Implements hook_views_data_alter().
 */
function guser_views_data_alter(&$data) {
  $data['users']['guser_edit'] = array(
    'field' => array(
      'title' => t('Guser edit link'),
      'help' => t('Provide a simple link to edit the user.'),
      'handler' => 'views_handler_field_guser_link_edit',
    ),
  );
}
