<?php
/**
 * @file
 * Group type configuration admin UI for site roles.
 */

/**
 * Add user role configuration to group type configuration.
 */
function guser_config_form($form, &$form_state, GroupType $group_type) {
  // Load the saved configuration.
  $defaults = isset($group_type->config['guser'])
    ? $group_type->config['guser']
    : array();

  $form['info'] = array(
    '#type' => 'item',
    '#title' => t('Site Roles'),
    '#description' => t('Select the site roles that group members with the proper permissions can manage.') . '<br />' . t('Group members that can manage user accounts will only be able to see/grant/remove these roles.'),
  );

  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#options' => user_roles(TRUE),
    '#default_value' => $defaults,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit handler for guser_config_form().
 */
function guser_config_form_submit($form, &$form_state) {
  $group_type = $form_state['build_info']['args'][0];

  // Extract the config settings and store them in the group type.
  $group_type->config['guser'] = array_keys(array_filter($form_state['values']['roles']));

  $group_type->save();
}
