<?php
/**
 * @file
 * Hook implementations for the Group module.
 */

/**
 * Implements hook_group_permission().
 */
function guser_group_permission() {
  $permissions = array(
    'create user' => array(
      'title' => t("Create user"),
      'description' => t("Create sitewide user accounts that are tied to this group."),
      'restrict access' => TRUE,
    ),
    'edit user' => array(
      'title' => t("Edit user"),
      'description' => t("Edit sitewide user accounts that are children of this group. Requires the 'Access the member overview page' permission."),
      'restrict access' => TRUE,
    ),
    'edit member user' => array(
      'title' => t("Edit member's user account"),
      'description' => t("Edit sitewide user accounts for all members in the group. Requires the 'Access the member overview page' permission."),
      'restrict access' => TRUE,
    ),
    'edit user roles' => array(
      'title' => t("Edit user roles"),
      'description' => t("Grants the ability to manage roles for sitewide user accounts."),
      'restrict access' => TRUE,
    ),
    'edit user status' => array(
      'title' => t("Edit user status"),
      'description' => t("Grants the ability to manage the status for sitewide user accounts."),
      'restrict access' => TRUE,
    ),
    'delete user' => array(
      'title' => t("Delete user"),
      'description' => t("Delete sitewide user accounts. An edit user permission is also needed for this permission to work."),
      'restrict access' => TRUE,
    ),
    'view user' => array(
      'title' => t("View user account profiles"),
      'description' => t("View sitewide user account profiles."),
      'restrict access' => TRUE,
    ),
  );

  return $permissions;
}
