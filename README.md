CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers
 
INTRODUCTION
------------
 
 Guser allows administrators of a Group entity to create, delete, and
 edit subusers and or members of the group. It aims to fully recreate
 the drupal user management system within groups so that administrators
 can be given permission to edit a selective group of users instead of
 all the users on a site.
 
  * For a full description of the module, visit the project page:
    https://www.drupal.org/sandbox/ctrladel/2557377
 
  * To submit bug reports and feature suggestions, or to track changes:
    https://www.drupal.org/node/add/project-issue/2557377
    
REQUIREMENTS
------------

This module requires the following modules:

 * Group (https://drupal.org/project/group)
 
RECOMMENDED MODULES
-------------------

 * gadd (https://drupal.org/project/group)
   When enabled, it allows a user to be created and added to a group in
   a single form.
   
INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
 See: https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.
   
CONFIGURATION
-------------

 * Download and enable the most recent version of Group.
 * Create a group type at Groups » Group types
     - admin/group/type
 * Enter the group type config and under 'Site User Roles' choose which 
   roles the group type can manage.
 * Add guser permissions to the group roles at 
     - Groups » Group roles(admin/group/role)
     - Groups » Group types > roles (admin/group/type/manage/MY_GROUP_TYPE/permissions/roles)
     - Any role with guser permissions will also need the 'View Group' permission.
   
MAINTAINERS
-----------

Current maintainers:

 * Kyle Einecker (ctrladel) - https://www.drupal.org/user/2824963
