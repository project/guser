<?php
/**
 * @file
 * Contains all router functions for the Group user module.
 */

/**
 * Implements hook_menu().
 */
function guser_menu() {

  $items['group/%group/user/create'] = array(
    'title' => 'Create user',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_register_form', 1),
    'access callback' => 'guser_access_callback',
    'access arguments' => array('create user', 1),
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  $items['group/%group/member/%user/edit-user'] = array(
    'title' => 'Edit user profile',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('user_profile_form', 3, 'account', 1),
    'access callback' => 'guser_access_callback',
    'access arguments' => array('edit user', 1, 3),
    'type' => MENU_LOCAL_TASK,
    'file' => 'user.pages.inc',
    'file path' => drupal_get_path('module', 'user'),
    'weight' => 20,
  );

  $items['group/%group/member/%user/cancel-user'] = array(
    'title' => 'Cancel user',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guser_cancel_confirm_form', 3, 'account', 1),
    'access callback' => 'guser_access_callback',
    'access arguments' => array('delete user', 1, 3),
    'file' => 'guser.pages.inc',
    'file path' => drupal_get_path('module', 'guser'),
    'weight' => 20,
  );

  $items['group/%group/member/user-management'] = array(
    'title' => 'Site Users',
    'page callback' => 'guser_user_admin',
    'page arguments' => array(1),
    'access callback' => 'guser_access_user_management_callback',
    'access arguments' => array(1),
    'file' => 'guser.pages.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
  );

  $items['group/%group/member/member-management'] = array(
    'title' => 'Group Members',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  );

  $items['admin/group/type/manage/%group_type/config/user-roles'] = array(
    'title' => 'Site User Roles',
    'description' => 'Configuration regarding the manageable sites roles.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guser_config_form', 4),
    'access callback' => 'user_access',
    'access arguments' => array('configure group module'),
    'file' => 'admin/guser.config.inc',
    'type' => MENU_LOCAL_TASK,
    'weight' => 5,
  );

  return $items;
}


/**
 * Implements hook_menu_alter().
 */
function guser_menu_alter(&$items) {
  $items['group/%group/member']['title callback'] = 'guser_member_page_title_callback';
  $items['group/%group/member']['title arguments'] = array(1);

  $items['user/%user']['access callback'] = 'guser_view_access';
}
